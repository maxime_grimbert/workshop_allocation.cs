# Répartition en ateliers

A outil générique en C# pour lire un fichier CSV* contenant une liste de personne avec des charactéristiques et produite une répartition de ces personnes entre des ateliers en se basant sur les préférences des personnes.

*Le format CSV a été choisi car il s'intègre facilement aux méthodes de travail pré-existantes de l'entreprise.

## Comment utiliser

- Télécharger le dossier "Release" ou le dépôt complet.
- Remplir le fichier CSV nommé "input.csv" qui se trouve dans le dossier "releases" (le CSV s'ouvre avec n'importe quel logiciel tableur) ou copier votre propre CSV directement dans ce dossier (en vérifiant bien que les intitulés de colonnes soient les mêmes).
- Sur Windows : lancer le programme en double-cliquant sur le fichier .exe.
- Ouvrir le fichier CSV intitulé "output.csv" et vous voila prêt à partager avec vos collègues le résultat de cette répartition entre ateliers.

# Workshop allocation

A generic C# tool to read a CVS* list of person with characteristics and output allocation of those persons into workshops based on their preference and some conditions.

* CVS was chosen for now to ease synergy with the company current workflow.

## How to use

- Download the folder called "releases" or the whole repo.
- Fill in the CVS file called "input.cvs" located in the "releases" folder (you can open it with any sheet program) or move your own cvs file in that folder (make sure to respect the column headers).
- For windows : launch the program by double-clicking the executable.
- Open the CVS file called "output.cvs" and you're ready to share the workshop allocation with your coworkers.
